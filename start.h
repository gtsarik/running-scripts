#ifndef START_H
#define START_H

#include <QWidget>
#include <QSize>
#include <QLayout>
#include <QPushButton>
#include <QProcess>
#include <QString>
#include <QStringList>
#include <QDir>
#include <QCheckBox>
#include <QList>
#include <QMessageBox>
#include <QFile>
#include <QIODevice>
//#include <QByteArray>
#include <QTextStream>
#include <QDateTime>
#include <QDebug>

#include <QPixmap>
#include <QApplication>
#include <QDesktopWidget>
#include <QFileDialog>

class Start : public QWidget
{
    Q_OBJECT

    QPushButton* btn_run;
    QPushButton* btn_close;
    QString msg_error;
    QString path_file;
    QStringList bat_list;
    QFile* file;

    QList<QCheckBox*> checkb_choice;
//    QCheckBox* checkb_choice;

public:
    Start(QWidget *parent = 0);
private:
    QProcess* process;

public slots:
    void startProcces();
    void processError(QProcess::ProcessError error);
};

#endif // START_H
