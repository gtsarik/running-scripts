#include "start.h"

Start::Start(QWidget *parent)
    : QWidget(parent)
{
    this->setLayout(new QVBoxLayout());

    msg_error = "";
    file = new QFile("log.dat");
    process = new QProcess(this);
    QVBoxLayout* lay_widget = new QVBoxLayout();
    QSize min_size(200, 0);
    btn_run = new QPushButton("Run", this);
    btn_close = new QPushButton("Close", this);
    path_file = "\\run";
    QDir dir(QDir::currentPath()+path_file);
    bat_list = dir.entryList(QStringList("*.bat"), QDir::Files);

    if (!file->exists()) {
        file->open(QIODevice::WriteOnly);
        file->close();
    }

    for (int i = 0; i < bat_list.size(); i++) {
        checkb_choice.append(new QCheckBox(bat_list[i], this));
        lay_widget->addWidget(checkb_choice[i]);
    }

    lay_widget->addWidget(btn_run);
    lay_widget->addWidget(btn_close);
    lay_widget->setSpacing(10);
    layout()->addItem(lay_widget);

    connect(btn_run, SIGNAL(clicked()),
            SLOT(startProcces()));
    connect(btn_close, SIGNAL(clicked()),
            SLOT(close()));
    connect(process, SIGNAL(error(QProcess::ProcessError)),
            this, SLOT(processError(QProcess::ProcessError)));

    this->setWindowTitle("Run Scripts");
    this->setMinimumSize(min_size);
}

void Start::startProcces()
{
    bool is_check = false;

    if (!checkb_choice.size()) {
        QMessageBox::information(0, "Information", "No files to run the script");
        return;
    }

    for (int i = 0; i < checkb_choice.size(); i++) {
        if (checkb_choice[i]->isChecked()) {
            QDateTime dt = QDateTime::currentDateTime();
            is_check = true;
            process->start(QDir::currentPath()+path_file+"\\"+
                                  checkb_choice[i]->text());
            process->waitForStarted();
            process->waitForFinished();

            msg_error += dt.toString("dd-MM-yyyy hh:mm:ss");
            msg_error += "\nRun script: " + checkb_choice[i]->text() + "\n";
            msg_error += QString(process->readAllStandardOutput()) + "\n";
            msg_error += QString(process->readAllStandardError());
            msg_error += "----------------------------------------------\n";
        }
    }

    if (!is_check){
            QMessageBox::information(0, "Information", "Select a file to run");
    }
    else {
        file->open(QIODevice::Append | QIODevice::Text);
        QTextStream out(file);
        out << msg_error;
        file->close();
    }
}

void Start::processError(QProcess::ProcessError error)
{
    QDateTime dt = QDateTime::currentDateTime();
    msg_error += dt.toString("dd-MM-yyyy hh:mm:ss");
    msg_error += "\nError Process:\n";
    msg_error += error;
    file->open(QIODevice::Append | QIODevice::Text);
    QTextStream out(file);
    out << msg_error;
    file->close();
    return;
}


