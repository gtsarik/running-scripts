#include "start.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    Start start(NULL);
    start.show();

    return app.exec();
}
