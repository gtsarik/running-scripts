#-------------------------------------------------
#
# Project created by QtCreator 2014-09-02T11:35:43
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = start
TEMPLATE = app


SOURCES += main.cpp\
        start.cpp

HEADERS  += start.h
